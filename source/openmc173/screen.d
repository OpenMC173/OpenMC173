module openmc173.screen;

interface Screen
{
    void onRenderLoop(float delta);
}
