module openmc173.game.game_camera;

import bindbc.raylib;
import exceeds_expectations;
import openmc173.raylib_ext.matrix;
import openmc173.raylib_ext.vector3;
import std.algorithm;
import std.conv;
import std.math;


final class GameCamera
{
    Vector3 position;
    Vector3 up;
    float fovy;

    real pitch;
    real yaw;

    invariant
    {
        expect(up.magnitude()).toApproximatelyEqual(1.0f);
    }

    this()
    {
        position = Vector3(0, 0, 0);
        pitch = -PI / 6;
        yaw = 5 * PI / 4;
        up = Vector3(0.0f, 1.0f, 0.0f);
        fovy = 70.0;

        SetCameraMode(asRaylibCamera(), CameraMode.CAMERA_CUSTOM);
    }

    public Vector3 dirOnXZ()
    out(result)
    {
        expect(result.magnitude()).toApproximatelyEqual(1.0f, 0.001f);
    }
    do
    {
        Matrix yawMtx = rotateY(this.yaw);
        Matrix pitchMtx = rotate(yawMtx.transform(Vector3(1, 0, 0)), this.pitch);
        Vector3 cameraDir = pitchMtx.transform(yawMtx.transform(Vector3(0.0f, 0.0f, -1.0f)));
        cameraDir.y = 0;
        return cameraDir.unit();
    }

    public void moveBy(Vector3 displacement)
    {
        position = position.add(displacement);
    }

    public void moveTo(Vector3 newPosition)
    {
        Vector3 displacement = newPosition.subtract(position);
        moveBy(displacement);
    }

    public Camera3D asRaylibCamera()
    {
        Matrix yawMtx = rotateY(this.yaw);
        Matrix pitchMtx = rotate(yawMtx.transform(Vector3(1, 0, 0)), this.pitch);
        Vector3 cameraDir = pitchMtx.transform(yawMtx.transform(Vector3(0.0f, 0.0f, -1.0f)));

        return Camera3D(
            position,
            position.add(cameraDir),
            Vector3(0.0, 1.0, 0.0),
            fovy,
            CameraProjection.CAMERA_PERSPECTIVE
        );
    }


    /// Rotates the camera about the Y axis by `angle` radians.
    public void rotateYaw(float angle)
    out(; this.yaw >= 0, "Yaw < 0: " ~ this.yaw.to!string)
    out(; this.yaw < 2 * PI, "Yaw >= 2π: " ~ this.yaw.to!string)
    do
    {
        this.yaw += angle;
        this.yaw %= 2 * PI;
        this.yaw += (this.yaw < 0) ? (2 * PI) : 0.0;
    }

    /**
     * Tilts the camera up or down by `angle` radians.
     *
     * The resulting pitch is clamped to the range [-π/2.001, π/2.001].
     * The boundaries are slightly smaller than ±(π/2) because pointing
     * the camera straight up or down leads to odd behavior.
     */
    public void rotatePitch(float angle)
    out(; this.pitch < PI / 2, "Pitch >= π/2: " ~ this.pitch.to!string)
    out(; this.pitch > -PI / 2, "Pitch <= π/2: " ~ this.pitch.to!string)
    {
        this.pitch += angle;
        this.pitch = this.pitch.clamp(-PI / 2.001f, PI / 2.001f);
    }
}
