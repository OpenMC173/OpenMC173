module openmc173.datatypes.section;

import bindbc.raylib;
import enumap;
import openmc173.datatypes.block;
import openmc173.datatypes.directions;
import openmc173.datatypes.ndarray;
import openmc173.datatypes.vector3i;
import openmc173.io.textures;
import openmc173.raylib_ext.rectangle;
import openmc173.raylib_ext.vector2;
import openmc173.util.memory;
import std.algorithm.searching : all;
import std.conv;
import std.experimental.allocator;
import std.experimental.allocator.mallocator;
import std.format;
import std.math : isNaN;


struct Section
{
    Vector3i position;
    NDarray!(Block, 16, 16, 16) blocks;
    Enumap!(CardinalDir, Section*) neighbours;

    Model opaqueModel;
    Model translucentModel;
    bool dirty;

    @disable this();

    this(
        Vector3i position,
        const ref NDarray!(Block, 16, 16, 16) blocks,
        Enumap!(CardinalDir, Section*) neighbours,
    )
    {
        this.position = position;
        this.blocks = blocks;
        this.neighbours = neighbours;
        this.dirty = true;
    }

    void initializeModel(Textures textures, Opacity opacity)
    in(opacity != Opacity.invisible, "initializeModel received Opacity.invisible, but there is no invisible model to initialize.")
    {
        Mesh mesh;

        Vector3[] vertices;
        Vector2[] texcoords;
        Vector3[] normals;
        ushort[] indices;
        Color[] colors;

        foreach (size_t x, size_t y, size_t z, Block block; blocks)
        {
            if (block.id == VanillaBlockIDs.AIR) continue;
            if (block.id >= blockInfo.length) continue;
            if (blockInfo[block.id].opacity != opacity) continue;

            struct ToRender
            {
                void function(
                    float x, float y, float z,
                    Rectangle textureUV,
                    ref Vector3[] vertices,
                    ref Vector2[] texcoords,
                    ref Vector3[] normals,
                    ref ushort[] indices,
                    ref Color[] colors
                ) addFace;
                bool isAtEdge;
                Block delegate() neighbourBlock;
                Section* neighbourSection;
                Block delegate() neighbourInNeighbour;
            }

            Enumap!(CardinalDir, ToRender) toRender;
            toRender.xNeg = ToRender(
                &addFaceXNeg,
                x == 0,
                () => blocks[x - 1, y, z],
                neighbours.xNeg,
                () => neighbours.xNeg.blocks[15, y, z]
            );
            toRender.xPos = ToRender(
                &addFaceXPos,
                x == 15,
                () => blocks[x + 1, y, z],
                neighbours.xPos,
                () => neighbours.xPos.blocks[0, y, z]
            );
            toRender.yNeg = ToRender(
                &addFaceYNeg,
                y == 0,
                () => blocks[x, y - 1, z],
                neighbours.yNeg,
                () => neighbours.yNeg.blocks[x, 15, z]
            );
            toRender.yPos = ToRender(
                &addFaceYPos,
                y == 15,
                () => blocks[x, y + 1, z],
                neighbours.yPos,
                () => neighbours.yPos.blocks[x, 0, z]
            );
            toRender.zNeg = ToRender(
                &addFaceZNeg,
                z == 0,
                () => blocks[x, y, z - 1],
                neighbours.zNeg,
                () => neighbours.zNeg.blocks[x, y, 15]
            );
            toRender.zPos = ToRender(
                &addFaceZPos,
                z == 15,
                () => blocks[x, y, z + 1],
                neighbours.zPos,
                () => neighbours.zPos.blocks[x, y, 0]
            );

            foreach (CardinalDir dir, ToRender tr; toRender)
            {
                bool shouldRenderThisFace = (
                    tr.isAtEdge ? (
                        tr.neighbourSection is null ? (
                            true
                        ) : (
                            opacity == Opacity.opaque ? (
                                !blockInfo[tr.neighbourInNeighbour().id].opacity == Opacity.opaque
                            ) : (
                                tr.neighbourInNeighbour().id == 0
                            )
                        )
                    ) : (
                        opacity == Opacity.opaque ? (
                            !blockInfo[tr.neighbourBlock().id].opacity == Opacity.opaque
                        ) : (
                            tr.neighbourBlock().id == 0
                        )

                    )
                );

                if (shouldRenderThisFace)
                {
                    tr.addFace(
                        (blocks.dimensions[0].to!int * position.x) + x.to!float,
                        (blocks.dimensions[1].to!int * position.y) + y.to!float,
                        (blocks.dimensions[2].to!int * position.z) + z.to!float,
                        textures.terrainUVs[block.id.to!ubyte][dir],
                        vertices,
                        texcoords,
                        normals,
                        indices,
                        colors
                    );
                }
            }
        }

        dirty = false;

        // Leave as Mesh.init
        if (vertices.length == 0) return;

        mesh.vertices = cast(float*) vertices.ptr;
        mesh.texcoords = cast(float*) texcoords.ptr;
        mesh.normals = cast(float*) normals.ptr;
        mesh.indices = indices.ptr;
        mesh.vboId = makeArray!(uint)(Mallocator.instance, 7).ptr;
        mesh.colors = cast(ubyte*) colors.ptr;

        mesh.vertexCount = vertices.length.to!int;
        mesh.triangleCount = (indices.length / 3).to!int;

        UploadMesh(&mesh, true);

        if (opacity == Opacity.opaque)
        {
            opaqueModel = LoadModelFromMesh(mesh);
            SetMaterialTexture(&opaqueModel.materials[0], MaterialMapIndex.MATERIAL_MAP_ALBEDO, textures.terrain);
        }
        else
        {
            translucentModel = LoadModelFromMesh(mesh);
            SetMaterialTexture(&translucentModel.materials[0], MaterialMapIndex.MATERIAL_MAP_ALBEDO, textures.terrain);
        }
    }


    Block setBlock(size_t x, size_t y, size_t z, Block block, Textures textures)
    in(x < 16 && y < 16 && z < 16, "setBlock expected coordinates [0..16, 0..16, 0..16] but got: [%d, %d, %d]".format(x, y, z))
    {
        Block oldBlock = blocks[x, y, z];
        blocks[x, y, z] = block;

        bool hasTranslucentNeighbour =
            (x != 0 && blockInfo[blocks[x - 1, y, z].id].opacity == Opacity.translucent) ||
            (x != 15 && blockInfo[blocks[x + 1, y, z].id].opacity == Opacity.translucent) ||
            (y != 0 && blockInfo[blocks[x, y - 1, z].id].opacity == Opacity.translucent) ||
            (y != 15 && blockInfo[blocks[x, y + 1, z].id].opacity == Opacity.translucent) ||
            (z != 0 && blockInfo[blocks[x, y, z - 1].id].opacity == Opacity.translucent) ||
            (z != 15 && blockInfo[blocks[x, y, z + 1].id].opacity == Opacity.translucent);

        if (blockInfo[oldBlock.id].opacity == Opacity.opaque) refreshModel(Opacity.opaque, textures);
        if (blockInfo[oldBlock.id].opacity == Opacity.translucent || hasTranslucentNeighbour) refreshModel(Opacity.translucent, textures);

        if (x == 0 && neighbours.xNeg !is null)
        {
            Opacity neighbourOpacity = blockInfo[neighbours.xNeg.blocks[15, y, z].id].opacity;
            if (neighbourOpacity != Opacity.invisible)
            {
                neighbours.xNeg.refreshModel(neighbourOpacity, textures);
            }
        }
        else if (x == 15 && neighbours.xPos !is null)
        {
            Opacity neighbourOpacity = blockInfo[neighbours.xPos.blocks[0, y, z].id].opacity;
            if (neighbourOpacity != Opacity.invisible)
            {
                neighbours.xPos.refreshModel(neighbourOpacity, textures);
            }
        }

        if (y == 0 && neighbours.yNeg !is null)
        {
            Opacity neighbourOpacity = blockInfo[neighbours.yNeg.blocks[x, 15, z].id].opacity;
            if (neighbourOpacity != Opacity.invisible)
            {
                neighbours.yNeg.refreshModel(neighbourOpacity, textures);
            }
        }
        else if (y == 15 && neighbours.yPos !is null)
        {
            Opacity neighbourOpacity = blockInfo[neighbours.yPos.blocks[x, 0, z].id].opacity;
            if (neighbourOpacity != Opacity.invisible)
            {
                neighbours.yPos.refreshModel(neighbourOpacity, textures);
            }
        }

        if (z == 0 && neighbours.zNeg !is null)
        {
            Opacity neighbourOpacity = blockInfo[neighbours.zNeg.blocks[x, y, 15].id].opacity;
            if (neighbourOpacity != Opacity.invisible)
            {
                neighbours.zNeg.refreshModel(neighbourOpacity, textures);
            }
        }
        else if (z == 15 && neighbours.zPos !is null)
        {
            Opacity neighbourOpacity = blockInfo[neighbours.zPos.blocks[x, y, 0].id].opacity;
            if (neighbourOpacity != Opacity.invisible)
            {
                neighbours.zPos.refreshModel(neighbourOpacity, textures);
            }
        }

        return oldBlock;
    }

    /++
    Unloads the model (opaque or translucent) and resets the corresponding field
    to Model.init, ready for re-initialization.
    +/
    void resetModel(Opacity opacity)
    in(opacity != Opacity.invisible, "resetModel received Opacity.invisible, but there is no invisible model to refresh.")
    {
        Model model = opacity == Opacity.opaque ? opaqueModel : translucentModel;
        UnloadModel(model);
        dirty = true;
        if (opacity == Opacity.opaque)
        {
            opaqueModel = Model.init;
        }
        else
        {
            translucentModel = Model.init;
        }
    }

    /// Reloads the model (opaque or translucent) instantly. Useful if you don't want the section to appear empty for a small part of a frame.
    void refreshModel(Opacity opacity, Textures textures)
    in(opacity != Opacity.invisible, "refreshModel received Opacity.invisible, but there is no invisible model to refresh.")
    {
        Model model = opacity == Opacity.opaque ? opaqueModel : translucentModel;
        UnloadModel(model);
        initializeModel(textures, opacity);
    }
}


enum VERTICES_PER_FACE = 4;

private void addFaceXPos(
    float x, float y, float z,
    Rectangle textureUV,
    ref Vector3[] vertices,
    ref Vector2[] texcoords,
    ref Vector3[] normals,
    ref ushort[] indices,
    ref Color[] colors
)
{
    makeOrExpand(Mallocator.instance, vertices, [
        Vector3(x + 1.0f, y,        z),
        Vector3(x + 1.0f, y + 1.0f, z),
        Vector3(x + 1.0f, y + 1.0f, z + 1.0f),
        Vector3(x + 1.0f, y,        z + 1.0f),
    ]);

    makeOrExpand(Mallocator.instance, texcoords, [
        textureUV.min.add(Vector2(textureUV.width, textureUV.height)),
        textureUV.min.add(Vector2(textureUV.width,                0)),
        textureUV.min.add(Vector2(              0,                0)),
        textureUV.min.add(Vector2(              0, textureUV.height)),
    ]);

    makeOrExpand(Mallocator.instance, normals, [
        Vector3(1.0f, 0.0f, 0.0f),
        Vector3(1.0f, 0.0f, 0.0f),
        Vector3(1.0f, 0.0f, 0.0f),
        Vector3(1.0f, 0.0f, 0.0f),
    ]);

    makeOrExpand(Mallocator.instance, indices, [
        (vertices.length - VERTICES_PER_FACE).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 1).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 2).to!ushort,
        (vertices.length - VERTICES_PER_FACE).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 2).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 3).to!ushort
    ]);

    makeOrExpand(Mallocator.instance, colors, [
        Color(153, 153, 153, 255),
        Color(153, 153, 153, 255),
        Color(153, 153, 153, 255),
        Color(153, 153, 153, 255),
    ]);
}

private void addFaceXNeg(
    float x, float y, float z,
    Rectangle textureUV,
    ref Vector3[] vertices,
    ref Vector2[] texcoords,
    ref Vector3[] normals,
    ref ushort[] indices,
    ref Color[] colors
)
{
    makeOrExpand(Mallocator.instance, vertices, [
        Vector3(x, y,        z),
        Vector3(x, y,        z + 1.0f),
        Vector3(x, y + 1.0f, z + 1.0f),
        Vector3(x, y + 1.0f, z),
    ]);

    makeOrExpand(Mallocator.instance, texcoords, [
        textureUV.min.add(Vector2(              0, textureUV.height)),
        textureUV.min.add(Vector2(textureUV.width, textureUV.height)),
        textureUV.min.add(Vector2(textureUV.width,                0)),
        textureUV.min.add(Vector2(              0,                0)),
    ]);

    makeOrExpand(Mallocator.instance, normals, [
        Vector3(-1.0f, 0.0f, 0.0f),
        Vector3(-1.0f, 0.0f, 0.0f),
        Vector3(-1.0f, 0.0f, 0.0f),
        Vector3(-1.0f, 0.0f, 0.0f),
    ]);

    makeOrExpand(Mallocator.instance, indices, [
        ((vertices.length) - VERTICES_PER_FACE).to!ushort,
        ((vertices.length) - VERTICES_PER_FACE + 1).to!ushort,
        ((vertices.length) - VERTICES_PER_FACE + 2).to!ushort,
        ((vertices.length) - VERTICES_PER_FACE).to!ushort,
        ((vertices.length) - VERTICES_PER_FACE + 2).to!ushort,
        ((vertices.length) - VERTICES_PER_FACE + 3).to!ushort
    ]);

    makeOrExpand(Mallocator.instance, colors, [
        Color(153, 153, 153, 255),
        Color(153, 153, 153, 255),
        Color(153, 153, 153, 255),
        Color(153, 153, 153, 255),
    ]);
}

private void addFaceYPos(
    float x, float y, float z,
    Rectangle textureUV,
    ref Vector3[] vertices,
    ref Vector2[] texcoords,
    ref Vector3[] normals,
    ref ushort[] indices,
    ref Color[] colors
)
{
    makeOrExpand(Mallocator.instance, vertices, [
        Vector3(x,        y + 1.0f, z),
        Vector3(x,        y + 1.0f, z + 1.0f),
        Vector3(x + 1.0f, y + 1.0f, z + 1.0f),
        Vector3(x + 1.0f, y + 1.0f, z),
    ]);

    makeOrExpand(Mallocator.instance, texcoords, [
        textureUV.min.add(Vector2(              0,                0)),
        textureUV.min.add(Vector2(              0, textureUV.height)),
        textureUV.min.add(Vector2(textureUV.width, textureUV.height)),
        textureUV.min.add(Vector2(textureUV.width,                0)),
    ]);

    makeOrExpand(Mallocator.instance, normals, [
        Vector3(0.0f, 1.0f, 0.0f),
        Vector3(0.0f, 1.0f, 0.0f),
        Vector3(0.0f, 1.0f, 0.0f),
        Vector3(0.0f, 1.0f, 0.0f),
    ]);

    makeOrExpand(Mallocator.instance, indices, [
        (vertices.length - VERTICES_PER_FACE).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 1).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 2).to!ushort,
        (vertices.length - VERTICES_PER_FACE).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 2).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 3).to!ushort
    ]);

    makeOrExpand(Mallocator.instance, colors, [
        Color(255, 255, 255, 255),
        Color(255, 255, 255, 255),
        Color(255, 255, 255, 255),
        Color(255, 255, 255, 255),
    ]);
}

private void addFaceYNeg(
    float x, float y, float z,
    Rectangle textureUV,
    ref Vector3[] vertices,
    ref Vector2[] texcoords,
    ref Vector3[] normals,
    ref ushort[] indices,
    ref Color[] colors
)
{
    makeOrExpand(Mallocator.instance, vertices, [
        Vector3(x,        y, z),
        Vector3(x + 1.0f, y, z),
        Vector3(x + 1.0f, y, z + 1.0f),
        Vector3(x,        y, z + 1.0f),
    ]);

    makeOrExpand(Mallocator.instance, texcoords, [
        textureUV.min.add(Vector2(              0,                0)),
        textureUV.min.add(Vector2(textureUV.width,                0)),
        textureUV.min.add(Vector2(textureUV.width, textureUV.height)),
        textureUV.min.add(Vector2(              0, textureUV.height)),
    ]);

    makeOrExpand(Mallocator.instance, normals, [
        Vector3(0.0f,-1.0f, 0.0f),
        Vector3(0.0f,-1.0f, 0.0f),
        Vector3(0.0f,-1.0f, 0.0f),
        Vector3(0.0f,-1.0f, 0.0f),
    ]);

    makeOrExpand(Mallocator.instance, indices, [
        (vertices.length - VERTICES_PER_FACE).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 1).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 2).to!ushort,
        (vertices.length - VERTICES_PER_FACE).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 2).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 3).to!ushort
    ]);

    makeOrExpand(Mallocator.instance, colors, [
        Color(109, 109, 109, 255),
        Color(109, 109, 109, 255),
        Color(109, 109, 109, 255),
        Color(109, 109, 109, 255),
    ]);
}

private void addFaceZPos(
    float x, float y, float z,
    Rectangle textureUV,
    ref Vector3[] vertices,
    ref Vector2[] texcoords,
    ref Vector3[] normals,
    ref ushort[] indices,
    ref Color[] colors
)
{
    makeOrExpand(Mallocator.instance, vertices, [
        Vector3(x,        y,        z + 1.0f),
        Vector3(x + 1.0f, y,        z + 1.0f),
        Vector3(x + 1.0f, y + 1.0f, z + 1.0f),
        Vector3(x,        y + 1.0f, z + 1.0f),
    ]);

    makeOrExpand(Mallocator.instance, texcoords, [
        textureUV.min.add(Vector2(              0, textureUV.height)),
        textureUV.min.add(Vector2(textureUV.width, textureUV.height)),
        textureUV.min.add(Vector2(textureUV.width,                0)),
        textureUV.min.add(Vector2(              0,                0)),
    ]);

    makeOrExpand(Mallocator.instance, normals, [
        Vector3(0.0f, 0.0f, 1.0f),
        Vector3(0.0f, 0.0f, 1.0f),
        Vector3(0.0f, 0.0f, 1.0f),
        Vector3(0.0f, 0.0f, 1.0f),
    ]);

    makeOrExpand(Mallocator.instance, indices, [
        (vertices.length - VERTICES_PER_FACE).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 1).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 2).to!ushort,
        (vertices.length - VERTICES_PER_FACE).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 2).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 3).to!ushort
    ]);

    makeOrExpand(Mallocator.instance, colors, [
        Color(204, 204, 204, 255),
        Color(204, 204, 204, 255),
        Color(204, 204, 204, 255),
        Color(204, 204, 204, 255),
    ]);
}

private void addFaceZNeg(
    float x, float y, float z,
    Rectangle textureUV,
    ref Vector3[] vertices,
    ref Vector2[] texcoords,
    ref Vector3[] normals,
    ref ushort[] indices,
    ref Color[] colors
)
{
    makeOrExpand(Mallocator.instance, vertices, [
        Vector3(x,        y,        z),
        Vector3(x,        y + 1.0f, z),
        Vector3(x + 1.0f, y + 1.0f, z),
        Vector3(x + 1.0f, y,        z),
    ]);

    makeOrExpand(Mallocator.instance, texcoords, [
        textureUV.min.add(Vector2(textureUV.width, textureUV.height)),
        textureUV.min.add(Vector2(textureUV.width,                0)),
        textureUV.min.add(Vector2(              0,                0)),
        textureUV.min.add(Vector2(              0, textureUV.height)),
    ]);

    makeOrExpand(Mallocator.instance, normals, [
        Vector3(0.0f, 0.0f, -1.0f),
        Vector3(0.0f, 0.0f, -1.0f),
        Vector3(0.0f, 0.0f, -1.0f),
        Vector3(0.0f, 0.0f, -1.0f),
    ]);

    makeOrExpand(Mallocator.instance, indices, [
        (vertices.length - VERTICES_PER_FACE).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 1).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 2).to!ushort,
        (vertices.length - VERTICES_PER_FACE).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 2).to!ushort,
        (vertices.length - VERTICES_PER_FACE + 3).to!ushort
    ]);

    makeOrExpand(Mallocator.instance, colors, [
        Color(204, 204, 204, 255),
        Color(204, 204, 204, 255),
        Color(204, 204, 204, 255),
        Color(204, 204, 204, 255),
    ]);
}
