module openmc173.datatypes.block;


struct Block
{
    ulong id;
}

enum VanillaBlockIDs : ubyte
{
    AIR = 0,
    STONE,
    GRASS,
    DIRT,
    COBBLE,
    PLANKS,
    SAPLING,
    BEDROCK,
    WATER_STILL,
    WATER_FLOW,

    LAVA_STILL,
    LAVA_FLOW,
    SAND,
    GRAVEL,
    ORE_GOLD,
    ORE_IRON,
    ORE_COAL,
    LOG,
    LEAVES,
    SPONGE,

    GLASS,
    ORE_LAPIS,
    BLOCK_LAPIS,
    DISPENSER,
    SANDSTONE,
    NOTEBLOCK,
    BED,
    RAIL_POWERED,
    RAIL_DETECTOR,
    PISTON_STICKY,

    COBWEB,
    TALL_GRASS,
    DEAD_SHRUB,
    PISTON,
    PISTON_HEAD,
    WOOL,
    UNKNOWN,
    FLOWER_YELLOW,
    FLOWER_RED,
    MUSHROOM_BROWN,

    MUSHROOM_RED,
    BLOCK_GOLD,
    BLOCK_IRON,
    DOUBLE_SLAB,
    SINGLE_SLAB,
    BRICK,
    TNT,
    BOOKSHELF,
    MOSSY_COBBLE,
    OBSIDIAN,

    TORCH,
    FIRE,
    MOB_SPAWNER,
    STAIR_WOOD,
    CHEST,
    REDSTONE_WIRE,
    ORE_DIAMOND,
    BLOCK_DIAMOND,
    CRAFTING_TABLE,
    CROP_WHEAT,

    FARMLAND,
    FURNACE_OFF,
    FURNACE_ON,
    SIGN_GROUND,
    DOOR_WOOD,
    LADDER,
    RAIL,
    STAIR_COBBLE,
    SIGN_WALL,
    LEVER,

    PRESSURE_PLATE_STONE,
    DOOR_IRON,
    PRESSURE_PLATE_WOOD,
    ORE_REDSTONE_OFF,
    ORE_REDSTONE_ON,
    REDSTONE_TORCH_OFF,
    REDSTONE_TORCH_ON,
    BUTTON_STONE,
    SNOW_LAYER,
    ICE,

    SNOW_BLOCK,
    CACTUS,
    CLAY,
    SUGARCANE,
    JUKEBOX,
    FENCE,
    PUMPKIN,
    NETHERRACK,
    SOUL_SAND,
    GLOWSTONE,

    PORTAL,
    JACK_O_LANTERN,
    CAKE,
    REPEATER_OFF,
    REPEATER_ON,
    CHEST_PHONY,
    TRAPDOOR,
}

enum Opacity
{
    opaque,
    translucent,
    invisible
}

struct BlockInfo
{
    Opacity opacity;
}

BlockInfo[] blockInfo = [
    VanillaBlockIDs.AIR:         BlockInfo(Opacity.invisible),
    VanillaBlockIDs.STONE:       BlockInfo(Opacity.opaque),
    VanillaBlockIDs.DIRT:        BlockInfo(Opacity.opaque),
    VanillaBlockIDs.COBBLE:      BlockInfo(Opacity.opaque),
    VanillaBlockIDs.PLANKS:      BlockInfo(Opacity.opaque),
    VanillaBlockIDs.LOG:         BlockInfo(Opacity.opaque),
    VanillaBlockIDs.GLOWSTONE:   BlockInfo(Opacity.opaque),
    VanillaBlockIDs.BLOCK_LAPIS: BlockInfo(Opacity.opaque),
    VanillaBlockIDs.GLASS:       BlockInfo(Opacity.translucent),
    VanillaBlockIDs.ICE:         BlockInfo(Opacity.translucent),
];
