module openmc173.datatypes.directions;

enum CardinalDir
{
    xPos, xNeg,
    yPos, yNeg,
    zPos, zNeg,
}
