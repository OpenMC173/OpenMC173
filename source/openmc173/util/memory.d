module openmc173.util.memory;

import core.runtime;
import std.conv;
import std.experimental.allocator;
import std.experimental.allocator.mallocator;
import std.range : isInputRange;


/// If `array` is null/uninitialized, allocate memory and fill it with `range`.
/// Otherwise, expand the array by `range.length` and fill it with `range`.
void makeOrExpand(T, Allocator, R)(auto ref Allocator alloc, ref T[] array, R range)
if (isInputRange!R)
{
    if (array is null)
    {
        array = makeArray(alloc, range);
    }
    else
    {
        if (!expandArray(alloc, array, range))
        {
            throw new Exception("Failed to expand array using Mallocator. Address: " ~ array.ptr.to!string ~ ", Contents: " ~ array.to!string);
        }
    }
}


/// To be called from destructors to ensure that it wasn't called as
/// part of a garbage collection cycle.
///
/// See Also:
///     https://p0nce.github.io/d-idioms/#GC-proof-resource-class
void assertNotInGC(alias obj)(string file = __FILE__, int line = __LINE__)
{
    import core.memory : GC;
    import core.stdc.stdlib : exit, free, malloc;
    import std.stdio : writefln, writeln;
    import std.string : startsWith;

    if (GC.inFinalizer)
    {
        // I tried to make this print a nice full stacktrace. After
        // lots of digging through druntime code, avoiding GC calls,
        // and copying private druntime code into this file, I finally
        // managed to get a stacktrace... only for it to print the
        // line that called assertNotInGC and then nothing but '???'.
        // Instead, I went with the easier option using __FILE__ and
        // __LINE__.
        string typeName = typeof(obj).stringof;
        writefln(
            "ERROR at %s:%d: Destruction of %s was triggered by the Garbage Collector. It needs to be freed manually.",
            file, line, typeName
        );
        exit(-1);
    }
}
